//canvas: espace graphique, geometrique, animation
/*
1- cree une fonction draw
2- cree une fonction pour recuperer la position de la sourie
*/

function draw() {
    
    const canvasDraw = document.getElementById("canvasDraw");
    const ctx = canvasDraw.getContext("2d")


ctx.fillStyle = "rgb(200,0,0)";
ctx.fillRect(10,10,50,50)


//


ctx.fillStyle ="rgba(200,100,200,0.8)";
ctx.beginPath();
ctx.moveTo(180, 150);
ctx.lineTo(100, 75);
ctx.lineTo(100, 150);
ctx.fill()

}
window.addEventListener("load",draw)

const art = document.getElementById("art")
const ctx = art.getContext("2d")

function getMousePos(e) {
    const rect = art.getBoundingClientRect()
    return {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top,
    }
}

function mouseMove(e) {

    const mousePos = getMousePos(e);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke() //faire une ligne
    ctx.strokeStyle = "red"; //faire un style de coeur. reflechire a une fonction/parametre de couleur
    ctx.lineWidth = 4; 

}

function mouseRemove(e) {  

    art.removeEventListener('mousemove', mouseMove)
}



art.addEventListener('mousedown', function (e) {
   //console.log( getMousePos(e));
   e.preventDefault(); //eviter drag&drop au click
   const mousePos = getMousePos(e);
   ctx.beginPath();
   ctx.moveTo(mousePos.x, mousePos.y);

   art.addEventListener('mousemove', mouseMove)
   art.addEventListener('mouseup', mouseRemove)
})

//Reset
reset.addEventListener("click", function () {  
    ctx.clearRect(0,0,800,400)
})
